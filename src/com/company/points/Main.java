package com.company.points;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);


        System.out.println("введите координаты точки x1:");
        int x1 = scan.nextInt();

        System.out.println("введите координаты точки y1:");
        int y1 = scan.nextInt();

        System.out.println("Желаете добавить еще? (1-да 2-нет)");
        int otv = scan.nextInt();

        if (otv == 1) {
            System.out.println("ваш выбор: 1");
            System.out.println("введите координаты точки x2:");
            int x2 = scan.nextInt();

            System.out.println("введите координаты точки y2:");
            int y2 = scan.nextInt();

            System.out.println("Желаете добавить еще? (1-да 2-нет)");
            int otv2 = scan.nextInt();

            if (otv2 == 2) {
                System.out.println("ваш выбор: 2");
                System.out.println("введите координаты центра окружности cx:");
                int cx = scan.nextInt();
                System.out.println("введите координаты центра окружности cy:");
                int cy = scan.nextInt();
                System.out.println("введите радиус:");
                int radius = scan.nextInt();

                Point p1 = new Point(x1, y1);
                Point p2 = new Point(x2, y2);
                Circle circle = new Circle(new Point(cx, cy), radius);

                if (circle.conteinsPoint(p1) && circle.conteinsPoint(p2)) {
                    System.out.println("В окружность попали такие точки:\n точка1 = x:" + x1 + ", y:" + y1 + "; \n точка2 = x:" + x2 + ", y:" + y2 + ";");
                } else if (circle.conteinsPoint(p1) && !circle.conteinsPoint(p2)) {
                    System.out.println("В окружность попали такие точки:\n точка1 = x:" + x1 + ", y:" + y1);
                } else if (!circle.conteinsPoint(p1) && circle.conteinsPoint(p2)) {
                    System.out.println("В окружность попали такие точки:\n точка2 = x:" + x2 + ", y:" + y2);
                } else {
                    System.out.println("В окружность не попала ни одна точка...");
                }

            } else {
                System.out.println("ваш выбор: 1");
                System.out.println("Но по условиям предусмотрено не более 2-х точек");
                System.out.println("введите координаты центра окружности cx:");
                int cx = scan.nextInt();
                System.out.println("введите координаты центра окружности cy:");
                int cy = scan.nextInt();
                System.out.println("введите радиус:");
                int radius = scan.nextInt();

                Point p1 = new Point(x1, y1);
                Point p2 = new Point(x2, y2);
                Circle circle = new Circle(new Point(cx, cy), radius);

                if (circle.conteinsPoint(p1) && circle.conteinsPoint(p2)) {
                    System.out.println("В окружность попали такие точки:\n точка1 = x:" + x1 + ", y:" + y1 + "; \n точка2 = x:" + x2 + ", y:" + y2 + ";");
                } else if (circle.conteinsPoint(p1) && !circle.conteinsPoint(p2)) {
                    System.out.println("В окружность попали такие точки:\n точка1 = x:" + x1 + ", y:" + y1);
                } else if (!circle.conteinsPoint(p1) && circle.conteinsPoint(p2)) {
                    System.out.println("В окружность попали такие точки:\n точка2 = x:" + x2 + ", y:" + y2);
                } else {
                    System.out.println("В окружность не попала ни одна точка...");
                }
            }

        } else {
            System.out.println("ваш выбор: 2");
            System.out.println("введите координаты центра окружности cx:");
            int cx = scan.nextInt();
            System.out.println("введите координаты центра окружности cy:");
            int cy = scan.nextInt();
            System.out.println("введите радиус:");
            int radius = scan.nextInt();

            Point p1 = new Point(x1, y1);
            Circle circle = new Circle(new Point(cx, cy), radius);

            if (circle.conteinsPoint(p1)) {
                System.out.println("В окружность попала:\n точка1 = x:" + x1 + ", y:" + y1 + ";");
            } else {
                System.out.println("В окружность не попала указанная точка...");
            }
        }
    }
}