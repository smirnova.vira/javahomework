package com.company.points;

public class Circle {
    private Point centr;
    private double radius;

    public Circle(Point centr, double radius) {
        this.centr = centr;
        this.radius = radius;
    }

    public Point getCentr() {
        return centr;
    }

    public void setCentr(Point centr) {
        this.centr = centr;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    //лежит ли в радиусе точка? да/нет?
    public boolean conteinsPoint(Point p)  {
        double distance = centr.distanceTo(p);
        return distance<= radius;

    }
}
