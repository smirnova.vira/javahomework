package com.company.human;

public class Main {
    public static void main(String[] args) {
        Human fio = new Human ("Смирнова", "Вера", "Анатольевна");
        fio.getFullName();
        fio.getShortName();

        Human fio2 = new Human ("Петров", "Иван");
        fio2.getFullName();
        fio2.getShortName();
    }
}
