package com.company.human;

public class Human {
    String surname;
    String name;
    String patronymic;

    public Human(String surname, String name, String patronymic){
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
    }
    public Human(String surname, String name) {
        this.surname = surname;
        this.name = name;
    }

    public void getFullName() {
        if (patronymic == null) {
            System.out.println(surname + " " + name);
        }  else {
            System.out.println(surname + " " + name + " " + patronymic);
        }
    }
    public void getShortName() {
        char N = name.charAt(0);

        if (patronymic == null) {
            System.out.println(surname + " " + N + ".");
        }  else {
            char P = patronymic.charAt(0);
            System.out.println(surname + " " + N + "." + " " + P + ".");
        }
    }
}
